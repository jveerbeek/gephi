---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---


# Getting started with Gephi

First, let’s open Gephi and start a new project! Click on your desktop Gephi desktop icon to open Gephi. If the installation went right, this is the screen you will see:

![Test](/images/screenshot_1.png)


Click on ```New Project```. Now you will be presented with this screen. Depending on your operating system yours might look a bit different, but everything should be there. If it's not, try `Window` in the menu bar to turn on `Graph`, `Appearance`, or other tabs.

![Test](/images/screenshot_2.png)
--
## Importing data

### Import edges
Now we are ready to import some data! Importing data can already be quite confusing in Gephi, as there are multiple ways to do it, and you are also allowed to import data multiple times (in multiple so-called "Workspaces"), but the most simple way to import the data is going to the menu bar (on the top of the screen), and click on ```File > Import Spreadsheet```. 

```{note}
The menu bar (where the options such as `File`, `Workspace`, `View`, `Tools` are located) is not included in the screenshots of this demonstration. For Windows users, it's part of the graphical interface. 
```

![Test](/images/screenshot_3.png)

Now, navigate to the folder where your data is located and find the file containing the **edges** (usually, 'edges' can be found in the filename), and click on `Open`. You'll now be presented with this screen:

![Test](/images/screenshot_4.png)

Some details of this screen might be different, depending on the dataset you have imported -- for example, you may, or may not, see an extra column with `Weight`. In general, however, your dataset really should always contain a `Source` and a `Target` column, when you import the edges file.

In these steps, you have to tell Gephi in what format your dataset is saved, and how to interpret certain columns in your dataset. For example, should a column containing numbers ranging from 2010 to 2020 be interpreted as dates, or as numbers? The answers to these questions depend, of course, on your dataset.

The most important decision you have to make on the screen shown above is the `Separator` parameter. There are essentially two ways to tell what separator to choose. The first is the trial and error method: try all four options and select the one where you don't see the following error:


```{error} 
Edges table needs a 'Source' and 'Target' column with node ids
```

[Meaning Gephi can't make sense of your dataset]

As there aren't many options, this will usually work the fastest. The second way to tell what seperator to choose is by opening your csv file in TextEdit(MacOS)/Notepad(Windows) and actually looking at the way the dataset is structured. The file we opened in the previous step, for example, looks like this in TextEdit: 

![Test](/images/screenshot_5.png)

As you can see, the columns are separated with a semicolon, meaning that those parameters should be selected in this case.

In this step the other two options, `Import as` and `Encoding`, can be left as if. `Import as` allows you to specify what type of file is imported. In this case, we're importing the edges file, so we select `Edges table`. The other option, `Encoding`, allows you to set the encoding of your csv file. Unless you see some strange characters appearing in the Preview, you can almost always leave this is as. Now click on `Next` to go to the next page:  

![Test](/images/screenshot_6.png)

Here, we need to select the data types. We already defined Target and Source in our .CSV file and Gephi thus reads this automatically; we now only have to tell Gephi how to interpret the other columns. 

```{admonition} What are data types?
*Integer*, *Float* and *Double* tell Gephi the column contains **numerical** values. *Integer* and *Float* are more easily and quicker processed, whereas *Double* is more precise and has more range. Target and Source are a *String*, which is **textual** value. These values are called “Data types”. If you are not sure which data type to select, have a look at the [Wikipedia page on data types](https://en.wikipedia.org/wiki/Data_type). 
```

In my personal experience, Gephi is pretty good at interpreting data types and in most cases, again, you can leave these parameters as is. When Gephi interprets your data wrongly, it's more likely that there are some erroneous entries in your dataset. When you checked whether all datasets make sense (i.e. numerical values are interpreted as integers/doubles and not as strings), click on `Finish`. 

![Test](/images/screenshot_7.png)


You will then see an Import report. This report lists issues if there are any, and you can alsosee how many edges and nodes will be created. If you encounter problems while importing your dataset, Gephi will list the issues and the rows in which the issue appears. You sometimes might have to go back to your spreadsheet to fix the issue. Make sure to do this before visualizing. 

However, we are not completely done with specifying our data types yet! We also need to select our network type (see the box below).   

```{admonition} Directed/undirected graph
In **undirected** graphs, edges have a two-way connection by default, it, therefore, does not matter which direction they are going. Some examples of undirected graphs are:

- Friends on Facebook
- LinkedIn connections
- Marital relations 
- Word co-occurences
- Network of colleagues


In **directed** graphs, the edges can have one-way and two-way connections, in which the direction *does* matter. You can send someone a letter, but that doesn't necessarily mean someone sends you a letter back. Some (other) examples include:

- Facebook/Twitter/Instagram likes/retweets
- E-mails
- Academic citations
```

Choose the network type relevant for your data, and click on `Append to existing workspace` instead of `New workspace`. Then click `OK`. 


Now your the edges file imported! If everything went OK, you will be presented with what you could call "the monolith of death":

![Test](/images/screenshot_8.png)

This is actually an innocent black square, representing the data that you have imported just now.

### Import nodes
Where the edges contain the most crucial elements to create a network visualization (the connections), we usually contain the nodes (and the metadata) in a separate file. So let's import the nodes-file as well (if you have one, that is)! 

To import the nodes file, again go the `File`, `Import spreadsheet...` only now go the location of the nodes file on your computer. When you found it and selected it, you are presented with the `General CSV options` screen. Make sure `Import as` is set to `Nodes table`.

You also have to decide on the `Seperator` and the `Charset` here again. Click on `Next` if everything is set, and in the next screen, make sure your variables in the next screen are interpreted correctly. If that's the case, click on `Finish`.

After you clicked on `Finish`, you are, again, presented with the `Import report`. Here, it is crucial to make sure to select `Append to existing workspace`, and not `New workspace`! Also, make sure `Graph Type` is set correctly. 

![Test](/images/screenshot_52.png)

Click on `OK`. And now all our data is imported! In the next section, we will discuss Gephi's interface in more detail. 