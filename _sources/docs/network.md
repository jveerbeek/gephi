---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---

(getting_started)=


# What is a network?

Gephi visualizes networks. But, what exactly is a network? Let’s have a look at the following example:

![](/images/network_1.png)

This network visualization shows interactions (lines) between people (circles). For example, `Luana` interacted with `Rico`, `Quin`, `John`, `Maria` and `Alex`. `Alex`, in turn, also interacted with `Hugo`, `Serena` and `Jet`. As these people interact with other, they form a network. However, `Michael` and `Joseph` only interacted with each other. They did not interact with others in the network. This thus means they are not directly connected with the main network, but only to each other.

In network analysis, the circles are often called **Nodes** and the connections between these nodes are called **Edges**. Sometimes edges also have **Weights**. These weights indicate the strength of the connection between nodes. This network has no weighted edges, but the connections between the nodes can go both ways. For example, you can see in the figure two lines coming from Luana and going to John. This means Luana interacted with John and John also interacted with Luana. You can also see that `Luana` and `John` have 2 lines. This means `Luana` interacted with `John`, and `John` also interacted with `Luana`. The interaction between `John` and `Elisa`, on the other hand, is only one-way directed.

This is a **directed** graph, but graphs can also be **undirected**. In undirected graphs, edges have a twoway connection by default, it therefor does not matter which direction they are going . In directed graphs, the edges can have one-way and two-way connections, in which the direction does matter.