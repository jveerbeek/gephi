---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---

(getting_started)=


# Gephi's interface
Now we have our data imported, it's good to first discuss the general interface of Gephi, as it  sometimes can be a bit counterintuitive. Here, we will focus on discussing the `Overview` and the `Data laboratory` panel. We will discuss the `Preview` panel later. Next, we are getting to the hands-on practice!



## The overview panel
The `Overview` panel is our primary interface to vizualize our network. Let’s dissect the different panels in this tab to understand what each one does:

![Test](/images/screenshot_9.png)

(If not all these windows are present, go to `Windows` in the menu bar to turn on `Graph`, `Appearance`, `Layout`, `Context`, and `Filters`. These are the windows you are going to use the most.) 

1. **Appearance** Here you can determine the color and size of the nodes, edges and labels. The
appearance is divided in two tabs: Nodes and Edges. These tabs have three other tabs:
Unique, Partition and Ranking.

2.  **Layout** Here you can choose the algorithm you use on the network which will give the network its shape. We will mainly use ForceAtlas 2, whivh will be explained later.

3. **Graph** Your network is visualized here (currently a black square, but this will change, don’t
worry!). Moreover, the left and lower menu bar allow you to make changes to the
visualization. Hold your right mouse button and drag your mouse to move around the graph.
Zooming in can be done by using the scroll-wheel.

4. **Context** Here the number of nodes and edges are displayed. With huge networks or when
you use filters, this will also show you the percentage of nodes and edges displayed in the
current visualization. For now, all are shown.

5. **Filters/statistics** You can use filters to filter out certain nodes, edges or attributes and
setting various parameters for these filters. In the statistics tab, you can apply algorithms for analyzing the dataset.

6. **Queries** Filters you used are displayed here and here you can furthermore configurate all
filters and apply them to the visualization.

## Data Laboratory
![Test](/images/screenshot_10.png)

The `data laboratory` contains your dataset (the nodes and edges we imported earlier). Slightly underneath the tabs, you will
see a ‘Nodes’ and ‘Edges’ tab. Click on those and observe the difference between these tabs.
At the edges table, you can once again see the Source and Target. Gephi
automatically created the columns Type and Id (which is a unique number). Depending on your dataset, you can also see the other columns you imported. We will be using
the Label column later on when exporting our visualization. In the lower bar there are various options for the columns. Here you can add a new column, delete and duplicate columns etc, or move on column to another.
