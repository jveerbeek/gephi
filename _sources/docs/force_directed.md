---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---


# (Force-directed) graph drawing


Now we are (somewhat) familiar with the interface of Gephi, let's go back to our little black monolith and finally begin actually performing the network analysis:

![Test](/images/screenshot_8.png)

As said, this represents our data, randomly divided over a square. In this constellation, the position of the nodes *isn't really meaningful*. You might be able to see this for yourself by hovering over some specific nodes in the black square with your mouse. When you hover over the network with your mouse, the other circles that are highlighted represent the connections to the circle your hoovering on. And as you will probably see, there doesn't seem to be some kind of relationship between the position of a certain node and the other nodes they are connected with. 

<div id="fig:lalune">
    
![Noconnection](/images/screenshot_11.png)

</div>

<!--
<div id="fig:lalune">
![A voyage to the moon\label{fig:lalune}](/images/screenshot_11.png)

</div>
-->

But if you have ever seen the output of a network analysis, you know that they do -- generally -- not look like black squares (or squares in general). In most network visualizations, the position of the little black circles, or the clusters of nodes, actually conveys *meaning*. For example, if we would have a network that is highly polarized, you might think of network visualization looking something like this:


![Test](http://allthingsgraphed.com/public/images/political-blogs-2004/left-right.svg)


And when you would hover over a node in the blue cluster, we might implicitly know that the probability of it having more connections to any of the nodes in the red cluster would be much lower than having more connection to the nodes in the blue cluster.

---
### What are force-directed graph drawing algorithms?
So how do we get to the point where the (visual) position of the nodes conveys meaning? Well, as you might have guessed based on the title of this chapter, the answer is to use a **(force-directed) graph drawing algorithm**. Typically, force-directed layout algorithms have two fundamental "rules":

1. Nodes repulse each other
2. Edges attract nodes closer to each other

Meaning that, in principle, all nodes are repulsed by each other, unless there is some connection between the nodes. In a correspondance network, I'm repulsed by everyone I didn't sent a letter (or didn't bother to send a letter to me [how dare they]!), but "attracted" to everyone I did. A force-directed graph drawing algorithm thus determines the position of the nodes in the network *relative* to other nodes in the network, and nodes with a lot of connections are put closer together. 

```{note}
Graph drawing algorithms are solely for the purpose on of *vizualisation* (and, subsequently, interpretation). It doesn't actually change your data, or change any of the network statistics we'll discuss in a later chapter.
```

There are **11** graph drawing algorithms avalaible in Gephi, some force-directed, others depending on some other heuristic (such as the Circular layout, which just draws nodes in circle based on metric you choose), but in this manual we'll focus on using the force-directed layout algorithm called **ForceAtlas2**. For an overview of other graph drawing layout algorithms, take a look [here](https://gephi.org/tutorials/gephi-tutorial-layouts.pdf). For a more in-depth explanation of ForceAtlas2, you can check out the [original paper](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0098679).

So why do we choose *ForceAtlas2*? Well, that's because *ForceAtlas2* excels at things we typically value when we apply a network analysis on data from the humanities: finding *clusters* and *complementarities*. The spatial position of the nodes can be easility interpreted by it's connection to the other nodes.   

```{admonition} Some citations: What is ForceAtlas2?
“ForceAtlas2 is a force directed layout: it simulates a physical system in order to spatialize a network. Nodes repulse each other like charged particles, while edges attract their nodes, like springs. These forces create a movement that converges to a balanced state. This final configuration is expected to help the interpretation of the data” ([Jacomy et al. 2014](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0098679))

“[ForceAtlas2 is] a highly iterative simulation of attracting and repulsing forces that makes ample use of the notion of ‘swinging’ (in a very literal sense) to find an ‘optimal’ position for nodes on the canvas without getting stuck in local optima” ([Röhle & Rieder 2017](https://mediarep.org/bitstream/handle/doc/13467/Datafied_Society_109-124_Rieder_Roehle_Digital-Methods_.pdf?sequence=5), 118).
```


### Running Force-Atlas2
Now let's finally get rid of the black square! Go to the `Layout` panel on the bottom left side on the screen, click on `---Choose a layout` and select `ForceAtlas 2`.

![Test](/images/screenshot_12.png)

Now click on `> Run`. 

![Test](/images/screenshot_14.png)

In the lower right corner you can see that the algorithm is running. You will immediately
see the visualization change after you’ve clicked on `Run`. After a couple of minutes, the
black square has changed into something like you see above.

To get a glimpse of the full network, it's good to zoom out: drag your mouse over to the `Graph` panel and scroll down until you see the full graph.

Note that *ForceAtlas 2* is an algorithm that doesn't have some final/finite state, so you'll have to tell Gephi yourself when to stop (this is especially good if your computer makes airplane-like sounds when running *ForceAtlas 2*, as mine does). In most cases, running the algorithm for a few minutes would suffice, and you can tell the program to stop running the algorithm when you don't see any *big* changes occurring in your network (when the algorithm is *converged*). You might still see the graph changing to *some* degree, but if the *general* structure of the network stays the same, you are ready to stop running the algorithm. After you click on `Stop`, you will see in the lower-left corner how many iterations have passed (a value around 10.000 would usually do).  


After you stopped running the algorithm and zoomed out, in most cases, you have something looking like this (though it depends on your dataset!):

![Test](/images/screenshot_17.png)

Circle 1), the inner circle, *usually* represents your *largest connected component(s)*; this is the part of the graph most nodes are connected to. As edges attract, these nodes hold on to each other very tightly. The outer circle, circle 2), contains the nodes that are not connected to the *largest connected component(s)*, and as nodes are repulsed by each other, these nodes drift further and further away from the center. 

Imagine, for example, a network of co-authorship of research articles. The largest connected component would probably represent the core of the research staff: the assistant professors, the professors, the postdoctoral researchers. You might have once written a paper together with a colleague, and that colleague has written another paper with another colleague, and that colleague with another colleague, and so on and so forth, so that, eventually, you are in some shape or form connected (through other nodes) to a colleague working in a completely different department. The nodes included in the outer circle, then, would be two students writing their first paper together, thus not being connected to anyone in the center of the network.

So what should we do with these nodes, with these "outliers"? Well, this depends on the nature of your research. If you are interested in how students enter the academic network, don't ignore them. But if you are mainly interested in what academic cliques (what researchers do frequently write papers together) are apparent in your dataset, you can probably ignore them. And as finding out what clusters are apparent in your network is often the goal of the network analysis, I tend to mostly "ignore" these nodes.

You can "ignore" these nodes now in two ways. The first is to just zoom in on the network and focus on your largest connected component. The second is to explicitly filter out the nodes that have less than $N$ connections or are not included in the largest connected component. We'll discuss the latter strategy in a later chapter, so let's just zoom in for now. 

![Test](/images/screenshot_18.png)

--- 
So what did the ForceAtlas 2 algorithm actually do? Recall that when our network was represented as a black box, [we didn't see any relationship between the position of a node and the nodes that they were connected with](#fig:lalune). 

So get a better intuition about what ForceAtlas 2 actually *does*, let's do the same thing again, and hoover over some nodes in the network:   

![Test](/images/screenshot_19.png)

For illustrative purposes, the node I've selected in the screenshot above is a node that is quite central in the network (and has a lot of connections with the other nodes). But in general, you will probably see that the other nodes that are connected with the node you are hoovering over (the nodes that are highlighted) are now actually relatively close! And that's exactly what *ForceAtlas 2* has done for us: positioning nodes that have a connection close to each other.  


### Configuring the parameters

When you selected `ForceAtlas 2` in the `Layout` panel, you might have seen you cannot only click on `Run`, but also configure some other parameters: 

![Test](/images/screenshot_15.png)

You can fully manipulate and change the graph by playing with the settings of the algorithm. While these settings may seem overwhelming at first, the main goal of tuning these settings is to make the final visualization as *readable* as possible. In some cases, you might want your nodes to not drift as far away as they do, or want to prevent your nodes from overlapping with each other.  

```{attention} The main goal of tuning the settings of ForceAtlas 2 is to make the final visualization as *readable* as possible. But be careful with changing these parameters too much, as they can completely change the interpretation of the graph. When you turn 'Stronger Gravity' on, for example, you'll probably be able to include all the nodes in one visualization, but some clusters might appear closer to each other than they "actually" are. In all cases, it's important to note down your settings and be transparent about these settings when you present your final visualization.  
```

```{tip}
When you want to go back to the default settings, click on `Reset` in the lower-left corner of the `Layout` panel.
```

To see what they these setting do, you can click on them in the `Layout` panel. The table below explains these settings in more detail.


|  Parameter   | Default   | Description
| :--- | ---: | ---: |
| Threads number | $3$  | **More threads means more speed if your cores can handle it** <br>This parameter is useful for faster convergence of ForceAtlas2, and can be particularly of use for processing large datasets. The higher, the faster; the value is usually set between $1-10$ (depending on what your computer can handle, and what other things you want to do besides running a network analysis).  | 
| Tolerance (speed) | $1.0$   | **How much swinging you allow. Above 1 discouraged. Lower gives less speed and more precision.**   <br> Usually set to default.   |
| Approximate Repulsion | <input type="checkbox" checked />   | **Barnes Hut optimization: n² complexity to n.ln(n) ; allows larger graphs.** <br> Usually set to default.  |
| Approximation  | $1.2$    | **Theta of the Barnes Hut optimization** <br> Usually set to default.    |
|Scaling| $2.0$ | **How much repulsion you want. More makes a more sparse graph.**![](/images/table_2.png) This setting doesn't actually do anything with general the structure of the network, only with the spacing between the nodes. Usually set between $2-200$, depending how much space you want between the nodes  |
|Stronger Gravity| <input type="checkbox" disabled /> |**A stronger gravity law** ![](/images/table_6.png) Stronger gravity means nodes are less likely to be dispersed. This can help when your dataset consists of widely divergent groups, but you still want to map these groups in one visualization (and it often gives your network a round shape). When a stronger gravity is set, this is usually combined with a lower *gravity* value (of $0.1$, for example)  ||
|Gravity| $1.0$ | **Attracts nodes to the center. Prevents islands from drifting away.** ![](/images/table_5.png) The lower the gravity, the more the nodes are attracted to the center of the graph. When *stronger gravity* is off, I usually set this to default, otherwise between $0.01-0.5$. |
|Dissuade hubs| <input type="checkbox" disabled /> |  **Distributes attraction along outbound edges. Hubs attract less and thus are pushed to the borders.** <br> This setting will push clusters more to the edges of the graph. | 
|LinLog mode| <input type="checkbox" disabled />| **Switch ForceAtlas’ model from lin-lin to lin-log (tribute to Andreas Noack). Makes clusters more tight.** | 
|Prevent Overlap| <input type="checkbox" disabled /> | **Use only when spatialized. Should not be used with “Approximate Repulsion”** ![](/images/table_3.png) Prevents overlap between the nodes, and can help your visualization get a more "clean" look. When you turn this setting on, you have to turn "Approximate Repulsion" off. |
|Edge Weight Influence| $1.0$ | **How much influence you give to the edges weight. $0$ is “no influence” and $1$ is “normal”.** ![](/images/table_4.png) |

To experience the effect of these parameters, you can first try to run ForceAtlas2 again but turn `Dissuade Hubs` on and set the `Scaling` setting to $1.5$, and see how to graph changes right before your eyes. Again, let the algorithm run until you see it looks like it has converged. Then click on `Stop`, turn `Prevent Overlap` on, and turn `Approximate Repulsion` off. Again, let it run for a while.    

<!--![Test](/images/screenshot_20.png)-->

![](/images/screenshot_45.png)

Now we've positioned the nodes in the network in a meaningful way using a force-directed graph drawing algorithm, we are ready to give the network some color! In the next section, we'll discuss community detection algorithms. 




