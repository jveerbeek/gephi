{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Layout and export\n",
    "Now the two most important parts of running a network analysis (graph drawing and community detection) are done, we are ready to focus on the somewhat more mundane but no less important part of creating a network visualization: the layout. \n",
    "\n",
    "To understand how to generally adjust the layout of the graph, we first need to go back to the (somewhat confusing) `Appearance` panel we briefly discussed in the previous chapter:\n",
    "\n",
    "![](/images/screenshot_36.png)\n",
    "\n",
    "Each time you want to change a specific part of the visualization, you have to essentially specify three choices. \n",
    "\n",
    "First (box #1), you have to specify whether to want to change the **nodes** (the black dots) or the **edges** (the connections). \n",
    "\n",
    "Second (box #2), you have to specify whether you want to adjust the **color** (the palette), the **size** (the circles), the **color of the labels** (the underlined A), or the **size of the labels** (the double TT). \n",
    "\n",
    "Finally (box #3), you have to specify the heuristic to adjust your visualization. **Unique** applies the same adjustment to all nodes/edges (i.e. all the nodes should be colored blue! or bigger! or with huge labels!); with **partition**, you have to specify a certain column in your dataset based on which your adjustments differ (for example, you want to give each department a different color in your co-authorship network, or want to color the communities, as we did in the last chapter), which is interpreted as a *categorical* value; and with **ranking** you also have to specify a certain column, but this is interpreted as a *numerical*/*ordinal* value (you want the size of the node to indicate how many citations someone has, for example).    \n",
    "\n",
    "```{warning} \n",
    "As also discussed in the previous chapter, the `Appearance` panel is sometimes not really responsive (on macOS, on Windows you get *some* feedback): you don't really see whether already clicked on `Nodes` or not, for example.\n",
    "\n",
    "You have to trust yourself, here.\n",
    "```\n",
    "\n",
    "## Node size\n",
    "Now we know the basics of the `Appearance` panel, let's put our theory to practice and try to adjust to change the size of the nodes based on the number of connections a certain node has (its `Degree`). \n",
    "\n",
    "To test your understanding of the `Appearance` panel, I will now leave it up to you how to get there (if you *really* want instructions, click on *click to show* below). \n",
    "\n",
    "```{toggle}\n",
    "Click on `Nodes`, then on the second icon (the circles), and finally on `Ranking`. Select `Degree` from the list, and change `Min size` to $5$ and `Max size` to 50. Click on `Apply` to apply these changes.\n",
    "\n",
    "![](/images/screenshot_37.png)\n",
    "\n",
    "```\n",
    "\n",
    "Once you got there, change the `Min size` to $5$ and the `Max size` to $50$ (though this depends on your data!). There aren't really strict guidelines for how to set these values, because the effect of these values both depends on 1) your data, and 2) the scaling you chose when running *ForceAtlas 2*. Generally, I tend to go for a range that makes the total visualization as *readable* as possible; for a range that makes nodes with a lot of interactions stand out, but also doesn't make the nodes with very few interactions invisible. \n",
    "\n",
    "After you click `Apply`, it can be good to run *ForceAtlas2* with the `Prevent Overlap` setting on again for a short moment. This is not because you have to totally reposition your nodes, but because changing the size of the nodes makes them overlap again. After you ran *ForceAtlas2* again, the nodes of your network should look something like this:\n",
    "\n",
    "![](/images/screenshot_38.png)\n",
    "```{tip} \n",
    "When your network is *directed*, you can also choose to size the nodes based on the *in-degree* or *out-degree*. See the next chapter for a discussion on these statistics.\n",
    "```\n",
    "\n",
    "## Adding labels\n",
    "As we discussed in the introduction of this chapter, the third and fourth icons of box #2 are for adjusting the labels of the network visualization.\n",
    "\n",
    "But hey, you might think, I don't see any labels at all! That's because we have to *add* them first before we can change the size of the labels... And hold on tight, because adding the labels is going to be quite the ride...\n",
    "\n",
    "First, we will take a drastic right turn by not going to the `Appearance` panel, but to `Data Laboratory`. We go there to specify what labels should be shown. For example, in a co-authorship network, you can choose to display the name of the authors in your network (which might be the most logical choice [though less GDPR proof]), but Gephi also allows you to display - say - the favorite animal of the authors (if you collected this, that is).\n",
    "\n",
    "In the `Data Laboratory`, click on `Nodes`, and then go to the `Label` column. In most cases, the label column is still empty. In other cases, however, this column is already specified. If this is the case, **skip this step**. If `Label` column is empty, we have to fill it. Determine the name of the column you want to copy to the `Label` column (this is often the column with the names of the nodes), and then go to `Copy data to other column`. Now, this sometimes still confusing to me, but after you click `Copy data to other column`, you have to select the column you want to copy to the `Label` column. So if you want to display `Birthyear` (though not advised) in the `Label` column, click `Birthyear` here. After you selected your column, select `Label` in the next window and click on `OK`. If everything went right, you will now see that the `Label` column isn't empty anymore.\n",
    "\n",
    "We'll take a left turn again and go back to the `Overview` panel, and try to find (and click on) the almost invisible button at the bottom right corner of the `Graph` tab:\n",
    "\n",
    "![](/images/screenshot_39.png)\n",
    "\n",
    "This opens an (unnamed) window (at the bottom of the `Graph` tab) where we *also* can change some elements of the appearance of the graph. To make the labels visible, make sure that `Labels` is selected, and turn `Node` on. This will probably produce a horrific output, similar to:\n",
    "\n",
    "![](/images/screenshot_40.png)\n",
    "\n",
    "To make the visualization look *slightly* better, you can adjust the `Size` slider below the checkbox you just clicked on. But even if you turn the slider all the way to the left, the visualization still looks rather messy. This is because we assigned a label to *every* node in the dataset. But in most cases, we do not really want that; we just want the most essential nodes to have a label, so that we can easily interpret the network. \n",
    "\n",
    "So how do we do that? Well, the method to do feels actually a bit hacky. First, we have to filter out the **nodes** (not the labels!) with less than $N$ interactions (the degree). To do that, go to `Filters`, click on `Topology` and drag `Degree Range` to `Drag subfilter here`. (If you don't see `Drag subfilter here`, double click on the filter you use earlier (here: `Partition (Modularity class)`); if you didn't select a filter earlier, just drag it to `Draw filter here`)\n",
    "\n",
    "![](/images/screenshot_41.png)\n",
    "\n",
    "After you dragged `Degree Range`, you will able to specify the range of the nodes you will include by dragging the sliders. Because we only want to include the most prominent nodes, only drag the slider to the left, not to the right. When you are specifying these, the nodes disappear, but we will retrieve them back later. Choose a minimum degree value that selects only a few nodes (less than 10) per community (make sure the filter is already turned on!). In my case, I choose a minimum degree value of $25$.\n",
    "\n",
    "After you selected your range, click on the button with showing an A with an arrow in the `Filters` tab:\n",
    "\n",
    "![](/images/screenshot_42.png)\n",
    "\n",
    "Now, all the labels of the nodes not included in this graph are removed. To get the nodes back, go to `Queries`, right-click on `Degree range` and select `Remove`. The nodes without a label now should have re-appeared on your screen:\n",
    "\n",
    "![](/images/screenshot_43.png)\n",
    "\n",
    "Now, we're almost done with the labels! For the final step, we can finally go back to our beloved `Appearance` panel to *size* the labels based on the *degree* of the nodes. Again, getting there this will be left as an exercise for you.\n",
    "\n",
    "```{toggle}\n",
    "Click on `Nodes`, then click on the icon showing the double T (the text size icon), then go to `Ranking`. Select `Degree` from the list.\n",
    "```\n",
    "\n",
    "Once you got there, change the `Min size` to $1$ and the `Max size` to $3$ (though this, again, depends on your data, and the size of your nodes, so play around with these settings until your result looks visually satisfying!). Now, your visualisation should look similar to this:\n",
    "\n",
    "![](/images/screenshot_44.png)\n",
    "\n",
    "## Exporting your graph\n",
    "When you feel like your graph is ready to be shown to the world, it's time to export it. To export the visualization, we have to go to a panel we haven't used before: the `Preview` panel.\n",
    "\n",
    "Now open the `Preview` tab, without touching any of the settings, click on the `Refresh`\n",
    "button. Great, we now have a first version to work with. In the `Preview` Settings menu you\n",
    "can customize settings concerning the Nodes, Edges, and Labels. There are multiple presets as\n",
    "well, which currently stands on default. Try out a few other ones and click on the Refresh\n",
    "button to see how it changes the graph. The default preset, however, will make your graph\n",
    "look something like this:\n",
    "\n",
    "![](/images/screenshot_46.png)\n",
    "\n",
    "As you can see, in my case, the font size of the labels is a little bit on the large side, so that's the first thing I'm going to change. To do that, click on the three dots (`...`) right to `Font`, and select a smaller font size (I'm going for $4.0$). \n",
    "\n",
    "If you want, you can try out different options. Play with the node labels, the opacity of edges and nodes to see what works best for you! You can also show other labels, for example, labels with a high degree, labels with a certain modularity or other measurements. Turn the labels on and off in the Data Laboratory.\n",
    "\n",
    "Personally, I like my network visualizations to have a black background. To do that, click on `Default` under `Presets`, and select `Black background` (no we need to change the size of the `Font` again..). To make the graph more readable, I also like to add a border around the labels that has the same color as the community nodes. To do that, go to `Outline color` and click on the three dots (`...`), and select `Parents`. Then, adjust the `Outline size` (I go for $10.0$). Now, the output of my visualisation looks like this:\n",
    "\n",
    "![](/images/screenshot_49.png)\n",
    "\n",
    "For now, we will end with this graph. We can export it, by click `SVG/PDF/PNG` at the bottom of the screen. SVG stands for Scalable Vector Graphic. I would advise you to save your file in all three file types. Saving it as a PDF will preserve all edges and nodes perfectly. Moreover, it allows for zooming in. The PNG file is not of good quality, therefore better used for just giving a quick overview. The best quality is either PDF or SVG.\n",
    "\n",
    "Now, this is what my PDF looks like when I open it:\n",
    "\n",
    "![](/images/screenshot_50.png)\n",
    "\n",
    "You can zoom in and out to explore the graph in Adobe Acrobat or any other PDF Reader."
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "text_representation": {
    "extension": ".md",
    "format_name": "myst"
   }
  },
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  },
  "source_map": [
   10
  ]
 },
 "nbformat": 4,
 "nbformat_minor": 4
}