---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---

(getting_started)=


# Network statistics



## Node-level statistics
In the previous chapter, we mainly worked with `Degree` to shape our network. We determined the size of the nodes based on `Degree` and only displayed the labels of the nodes with a `Degree` higher than 25. Now in many cases, `Degree` is a logical choice for shaping your network. But in other cases, it can be a bit deceiving. 

Imagine for example a Twitter network, where one user constantly spams other users, spending his day doing nothing else than constantly interacting with other users... but never getting a reply back! If we choose `Degree` to visualize our network, this user would appear a very crucial figure in the network, but he's mainly a very *active* user ([or even a bot](https://www.dvhn.nl/drenthe/Koning-van-de-Nederlandse-twitterbots-stuurde-ruim-120.000-beledigende-berichten-aan-politici.-Het-account-is-te-linken-aan-bedrijf-uit-Drenthe-dat-vermoedelijk-slachtoffer-werd-van-een-hack-26652911.html)). So when we are not particularly interested in the most active users, but rather, in the most influential ones, we might want to opt for other metrics to shape our network.     

Below, I provide an overview of all the relevant node-level network statistics. You can use these statistics to shape your network visualization (determine the size of the nodes, filter out certain nodes, determine the labels), but running these statistics on your network also allows you to display them in a separate table. For example, Van der Deijl and Smeets (2019), include a table with the nodes with the highest `PageRank` value to say something about the canonicity of these nodes. 

In general, to get an overview of the nodes with these highest values of certain statistics, go to `Data laboratory`, `Nodes`, and click twice on the column name (for example `In-degree`) you want to sort your nodes by. You can also drag and drop the columns themselves to create a better overview. For example, if you sort the nodes by `In-degree`:

![](/images/screenshot_51.png)

---

### Average degree
First, while you probably already have done this if you followed the tutorial, to get the `Degree`, `In-degree` and the `Out-degree` of the nodes (or only the `Degree` if your network is undirected), go to `Statistics`, and run `Average degree`. In your `Data Laboratory` tab, you will now see that the columns giving the degrees have been added.

#### Degree
Degree is the total number of interactions a node has. If I, in total, have sent five letters and received two, my degree is seven.

#### In-degree
The in-degree measures the total number of interactions a node has received (for which it is the **target**). If I, in total, have sent five letters and received two, my in-degree is two.

#### Out-degree
The out-degree measures the total number of interactions a node has sent (for which it is the **source**). If I, in total, have sent five letters and received two, my out-degree is five.

---

### Network diameter
To get the `Eccentricity`, `Betweenness Centrality` and the ` Closeness Centrality` of the nodes, go to `Statistics`, and run `Network diameter`. In your `Data Laboratory` tab, you will notice that the columns giving these statistics have been added.

#### Eccentricity 
Eccentricity entails the distance of a node to the node furthest from it. A high value suggests that this distance is large, whereas a low eccentricity means the distance is small. This also means the network diameter is the max eccentricity among all the nodes.

#### Betweenness Centrality
![](https://sites.google.com/site/bsmithactivity3/_/rsrc/1312829093923/betweenness-centrality/betweenness.jpg?height=347&width=400)

This measurement is based on the number of shortest paths between two nodes. It is an indicator of the centrality or importance of a node in a network. A high value suggests that a node is connecting parts of a network together. A lower value means that nodes are not central in the network.

#### Closeness Centrality
This measure indicates the **closeness** of a node to other nodes. A higher value means the average distance from a node to other nodes in the network is larger. A lower value means the average distance is shorter. It might be an indicator at the speed at which information flows through the network.

---

### PageRank
PageRank is an algorithm (and a measure!) that was proposed by the founders of Google and originally used to rank Google's search results. Compared to a measure like in-degree, PageRank does not only look at how *many* other nodes interact with a certain node but also incorporates how *popular* the nodes that interact with a certain node are. In the example below (from Wikipedia):

![](https://upload.wikimedia.org/wikipedia/commons/thumb/f/fb/PageRanks-Example.svg/1280px-PageRanks-Example.svg.png)

You see, for example, that node $B$ has a relatively high PageRank, mainly because this node has a lot of other nodes that interact with $B$. But while node $A$ and node $C$ both only have one incoming edge, $A$ has a much higher value. This because its incoming edge of $A$ comes from node $B$, which is also very "popular", while $A$ incoming edge is from node $D$.  The PageRank value is a **proportion**, where the sum of the proportion over all the nodes is equal to 1.

To get the PageRank values for each node, go to `Statistics`, `PageRank`, and then `Run`. You can leave to settings to Default (but do check whether you selected the right type (directed/undirected) of network). Then click `OK`.

---

## Network-level statistics
The above measures are all calculated on node level and can be used for visualization purposes. In some cases, you also want some statistics on a *network* level to help you interpret the complete network. When you have one network, these statistics could be quite hard to interpret, but when you want to compare a series of networks (for example: could we see evidence of polarisation on social media over the last ten year?), these statistics will tell you how your network changes without having to make a network visualisation (and interpret it) for each point in time.

### Modularity 
See chapter 'Community detection'. Modularity measures how well your network can be divided into groups.

### Density
Network density shows the extent to which a network is **connected**. Density calculated by the ratio between the number of existing links (edges) and all possible links are given the number of nodes. The range of density is between 0 and 1, where 0 means that a network has no connection and 1 means a network is fully connected.

![](https://methods.sagepub.com/images/virtual/sage-encyclopedia-of-educational-research-measurement-evaluation/10.4135_9781506326139-fig112.jpg)

The network on the left is fully connected, and therefore has a high density, while the network on the right has a low density.

To calculate the density of a network, go to `Statistics`, `Graph Density`, click on `Run`, select the type of your network (directed/undirected) and click `OK`. The graph density is now displayed.

### Average shortest path
"Shortest path indicates how far two given nodes are from each other. If any two nodes in a network are close to each other, it means that it is easy to reach any node in a network from any other given node. Such structures are very robust and well connected."

To calculate the density of a network, go to `Statistics`, `Avg. path length`, click on `Run`, select the type of your network (directed/undirected) and click `OK`.


### Clustering coefficient
"This measure indicates the degree to which nodes in a graph create densely connected clusters. The CC for a network is calculated as an average clustering over all nodes in a network."

To calculate the clustering coefficient of a network, go to `Statistics`, `Avg. Clustering Coefficient`, click on `Run`, select the type of your network (directed/undirected) and click `OK`.


