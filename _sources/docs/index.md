# Introduction in network analysis in Gephi

This manual will explain how to visualize a dataset in Gephi and how to gain interesting insights from this visualization. If you are completely new to network analysis: don’t worry, this tutorial will (hopefully) guide you through every step with the use of helpful screenshots and textual directions. If you have some experience with Gephi already, then this tutorial will also be helpful as it will likely show you some ins and outs you didn’t know (though you can probably skip a few steps).

```{note}
Parts of this tutorial were taken from Utrecht Data School's 'Gephi Tutorial: Vizualizing and exploring networks' written by Marjolein Krijgsman. 
```

## What is Gephi?
Gephi is an interactive network analysis and visualization tool, developed by Mathieu Bastian, Eduardo Ramos Ibañez, Mathieu Jacomy, Cezary Bartosiak, Julian Blicke, Patrick McSweeney, André Panisson, et al. (2017, version 0.9.2). On their website, they state:

"Gephi is a tool for data analysts and scientists keen to explore and understand graphs. Like
Photoshop™ but for graph data, the user interacts with the representation, manipulate the structures, shapes, and colors to reveal hidden patterns. The goal is to help data analysts to make hypothesis, intuitively discover patterns, isolate structure singularities or faults during data sourcing. It is a complementary tool to traditional statistics, as visual thinking with interactive interfaces is now recognized to facilitate reasoning. This is a software for Exploratory Data Analysis, a paradigm appeared in the Visual Analytics field of research (“Features”, Gephi.org). 


Gephi visualizes your data in real-time, which means you can see the graph take shape right in front of your eyes! It is used to explore and visualize relational data, or networks..
